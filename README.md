# Micro Challenge II MDEF Personal Reflection
#### Personal/Individual Reflection on the Micro Challenge II

## **Initial Questions:**
#### 1. What Are The Main Causes and Influencing Factors of Malnutrition & Health Issues Globally?
#### 2. Where is Vertical Farming Done in Spain and in Barcelona Specifically?
#### 3. Who Produces and Sells Microgreens Here in Barcelona?
#### 4. How to Make a Healthy Soda/Carbonated Drink?
#### 5. Could Microgreens Be a Potential Solution in Addressing The Huge Problems Mentioned on Question 1?

## **Facts: An Objective Account of What Happened**

#### For this challenge, I teamed up with Jean-Luc Pierite. I proposed creating some sort of healthy and nutritious soda/carbonated drink, after reading a very specific quote from the Global Nutrition Report 2018: _About a third (30.3%) of school-aged children do not eat any fruit daily, yet 43.7% consume soda every day._ We got inside the classroom at the end of the day, and started brainstorming everything. There were many other big problems that were "added to the mix", such as food waste, health issues (obesity, diabetes, cardiovascular disease,etc.), the ridiculously excessive water and energy footprint coming from the agriculture industry, the overconsumption of "empty" energy drinks (artificial/synthetic drinks with fake vitamins and minerals in it), lack of local food production and plastic pollution. Bearing this in mind, we conceptualized the first prototype of the drink: microgreens, dehydrated fruit powder (from food waste) and a natural sweetener (like stevia). The other part of the project consisted of making the structure that would hold all of this. We decided for a cabinet-like structure with three levels. This would be done with the full aid of a laser cutter and a CNC machine. The first (top) level would house the microgreens, complete with a LED grow light. The second (middle) level would house the natural sweetener (stevia) and the carbonating agent (sodium bicarbonate). The third (bottom) level would house the dehydrating chamber, complete with a heating lamp and a ventilation fan, plus a showcase of the finely ground orange peel powder that would be added to the Planetary Soda. 

## **Feelings: The Emotional Reactions to the Situation**

#### I felt like a visionary throughout the whole process. From initially stumbling totally by coincidence into the Global Nutrition Report, reading about a particular problem on a section of it, spontaneouly coming up with the Planetary Soda concept "right there on the spot", and finally putting all the work required to turn it into reality. There's no better feeling than that!! I also felt very glad and happy to work with Jean-Luc, I felt that were truly were the perfect team, a lot like Steve Jobs and Steve Wozniak :)

## **Findings: The Concrete Learning That You Can Take Away From the Situation**

#### There were many lessons for me while doing this micro challenge. The biggest of all was the realization that talent is never enough to do anything. Many times we fool ourselves into believing and thinking that just because we have a certain natural ability/inclination into doing something, it will just happen magically by itself. But by executing this idea with my colleague, I learned there's a lot of work, effort and iteration needed to make it happen. Much more than I ever thought it would be necessary...

## **Future: Structuring Your Learning Such That You Can Use it in the Future**

#### After the end of this process, I felt like I got a much better and deeper understanding of how technology works, specially electronics. It didn't seem like a "black box" anymore to me. I got to learn and understand many basic principles, mainly the function of _voltage_, _ground_, _AC/DC_, the _GPIO Pinout_ on the ESP32, how to wire a fan or a lamp, and much much more... Now I feel much more comfortable and eased up when I go through the Fab Academy content by myself. 






